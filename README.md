## **Projeto AUgenda**
* Projeto que visa melhorar a qualidade de vida do seu pet.
* O mesmo será um auxilio ao usuário para a administração saudavel de seu pet.
* Repositório com levantamento de requisitos [aqui!](https://gitlab.com/senac-projetos-de-desenvolvimento/alexadre-cardoso-peres/AUgenda/-/wikis/Requisitos-Funcionais/WIki-Augenda)

    ## **Informações**
    > * Alexandre C Peres - Dev
    > * Angelo Luz - Prof
    
    >* **Toda informação está sujeita a modificações ao decorrer do desenvolvimento do produto final.**
